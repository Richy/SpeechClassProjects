# -*- coding: utf-8 -*-
# @Author: Heinrich Dinkel
# @Date:   2017-12-21 09:50:55
# @Last Modified by:   Heinrich Dinkel
# @Last Modified time: 2017-12-21 14:16:35
import torch
import numpy as np
# Frame extension
from sklearn.feature_extraction import image


class Listdataset(torch.utils.data.Dataset):
    """docstring for Listdataset"""

    def __init__(self, datalist, labels, fext=0):
        super(Listdataset, self).__init__()
        assert len(datalist) == len(labels)
        self.datalist = datalist
        self.labels = labels
        self.fext = fext

    def __getitem__(self, index):
        return frameextension(self.datalist[index], self.fext), self.labels[index]

    def __len__(self):
        return len(self.datalist)


def coll(batch):
    batches, labs = [], []
    for b in batch:
        batches.append(b[0])
        labs.extend(b[1])
    return torch.from_numpy(np.concatenate(batches)), torch.from_numpy(np.array(labs))


class CacheDataLoader(object):
    def __init__(self, dataset, cachesize, batchsize, shuffle):
        super(CacheDataLoader, self).__init__()
        self.dataset = dataset
        self.cachesize = cachesize
        self.batchsize = batchsize
        self.shuffle = shuffle
        self.nsamples = sum(sample[0].shape[0] for sample in self.dataset)

    def __iter__(self):
        cache = torch.utils.data.DataLoader(
            self.dataset, shuffle=self.shuffle, batch_size=self.cachesize, collate_fn=coll)
        for k, v in cache:
            batchdata = torch.utils.data.TensorDataset(k, v)
            batchloader = torch.utils.data.DataLoader(
                batchdata, shuffle=self.shuffle, batch_size=self.batchsize)
            for batchdata, batchlabel in batchloader:
                yield(batchdata, batchlabel)

    def __len__(self):
        return self.nsamples // self.batchsize


def frameextension(data, fext):
    fextdim = fext * 2 + 1
    if fextdim == 1:
        return data
    # The extract patches method will not pad the first n and last n rows
    paddeddata = np.empty(
        (data.shape[0] + fext * 2, data.shape[1]), dtype=np.float32)
    paddeddata[:fext, :] = data[0]
    paddeddata[-fext:, :] = data[-1]
    paddeddata[fext:-fext, :] = data
    # Lazy mans frame extension, still fast enough!
    # Pads the input data with its adjacent (fextdim,fextdim) frames
    return image.extract_patches_2d(
        paddeddata, (fextdim, paddeddata.shape[-1])).reshape(-1, fextdim * paddeddata.shape[-1])
