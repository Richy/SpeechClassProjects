

# Backend
 
How to use features to decide between real and spoofed?
 
# Previous Work
 
- direct classification using GMM/SVM/...
- representation extraction from DNN's hidden layers, then GMM/SVM/...
 
# Direct CNN (DCNN)
 
*Small-Footprint Convolutional Neural Network for Spoofing Detection*
 
Heinrich Dinkel, Yanmin Qian, Kai Yu. IJCNN 2017
 
# Direct CNN (DCNN) - Concept
 
- DNN for scoring, not just representation
- convolutional layers for dimension increase: separability
 
# Direct CNN (DCNN) - Architecture
 
![DCNN architecture](backend_slides/dcnn_architecture.png)
 
only $\approx$ 100,000 parameters
 
# Direct CNN (DCNN) - Scoring
 
- model outputs probability for each input frame
- reduction to score for whole utterance: mean or variance
 
# Direct CNN (DCNN) - Results
 
![equal error rate (EER) on ASVSpoof2015](backend_slides/dcnn_results.png)
 
# Convolutional Long-Short Term DNN (CLDNN)
 
*End-to-End Spoofing Detection with Raw Waveform CLDNNs*
 
Heinrich Dinkel, Nanxin Chen, Yanmin Qian, Kai Yu. ICASSP 2017
 
# Convolutional Long-Short Term DNN (CLDNN) - Concept
 
- learn time-frequency transform on raw waveform
- LSTM for sequence reduction
- DNN classification of final LSTM output
- end-to-end training
 
# Convolutional Long-Short Term DNN (CLDNN) - Architecture
 
![CLDNN architecture](backend_slides/cldnn_architecture.png){width=230px}
 
# Convolutional Long-Short Term DNN (CLDNN) - Results
 
![half total error rate on BTAS2016](backend_slides/cldnn_results.png)
 
